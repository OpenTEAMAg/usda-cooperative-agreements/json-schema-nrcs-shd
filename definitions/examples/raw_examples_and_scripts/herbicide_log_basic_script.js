const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let herbicideConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--input--herbicide/object.json` ) );
let herbicideConvention = new builder.ConventionSchema( herbicideConventionSrc );

// reading the example
let herbicideExample = exampleImporter(filename = "herbicideLogSurvey_basic.json");

Object.keys(herbicideConvention.schema.properties);

// empty object
let formattedExample = {
    herbicide_log: {},
    plant_asset: {},
    rate: {},
    rate_unit:{},
    area_quantity:{},
    area_unit:{},
    herbicide_name: {},
    log_category: {}
};

//the first entity in the convention is the area quantity, assigning that here
formattedExample.area_quantity = herbicideExample[0];

//second entity is the area unit (%)
formattedExample.area_unit = herbicideExample[1];

//third entity is rate
formattedExample.rate = herbicideExample[2];

//fourth entity is a taxonomy_term--material_type
formattedExample.herbicide_name = herbicideExample[3];

//fifth entity is herbicide log
formattedExample.herbicide_log = herbicideExample[4];

//sixth entity is plant asset
// We need to fill in all the fields that are mandatory before adding it.
herbicideExample[5].attributes.name = "tacit_plant_asset";
herbicideExample[5].attributes.status = "active";
formattedExample.plant_asset = herbicideExample[5];

//seventh entity is log category
formattedExample.log_category = herbicideExample[6];

// testing the example
herbicideConvention.validExamples = [ formattedExample ];

let test = herbicideConvention.testExamples();

const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let solarizationConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--activity--solarization/object.json` ) );
let solarizationConvention = new builder.ConventionSchema( solarizationConventionSrc );

// reading the example
let solarizationExample = exampleImporter(filename = "solarizationLogSurvey_basic.json");

Object.keys(solarizationConvention.schema.properties);

// empty object
let formattedExample = {
    solarization_log: {},
    plant_asset: {},
    area_quantity:{},
    area_unit:{},
    log_category: {}
};

//the 1st entity in the convention is the area quantity, assigning that here
formattedExample.area_quantity = solarizationExample[0];

//the 2nd entity in the convention is the area unit (%)
formattedExample.area_unit = solarizationExample[1];

//the 3rd entity in the convention is the solarization log
formattedExample.solarization_log = solarizationExample[2];

//the 4th entity in the convention is the log category
formattedExample.log_category = solarizationExample[3];

//the 5th entity is also log category, can we still assign to log category? (have both weed_control[3] and termination[4]) 
//I think we are ok to leave this out based on Octavio's advice from the irrigation log

//all entities repeat themselves 

// testing the example
solarizationConvention.validExamples = [ formattedExample ];

let test = solarizationConvention.testExamples();

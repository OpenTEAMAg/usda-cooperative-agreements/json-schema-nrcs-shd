const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let flamingConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--activity--flaming/object.json` ) );
let flamingConvention = new builder.ConventionSchema( flamingConventionSrc );

// reading the example
let flamingExample = exampleImporter(filename = "flamingLogSurvey_basic.json");

Object.keys(flamingConvention.schema.properties);

// empty object
let formattedExample = {
    flaming_log: {},
    log_category: {},
    area_quantity: {},
    area_unit: {},
    plant_asset:{}
};

//the first entity in the convention is the flaming log, assigning that here
formattedExample.flaming_log = flamingExample[0];

//second entity is the plant asset
// We need to fill in all the fields that are mandatory before adding it.
flamingExample[1].attributes.name = "tacit_plant_asset";
flamingExample[1].attributes.status = "active";
formattedExample.plant_asset = flamingExample[1];

//third entity is log category
formattedExample.log_category = flamingExample[2];

// this is the basic example so the area and area units are not included

// testing the example
flamingConvention.validExamples = [ formattedExample ];

let test = flamingConvention.testExamples();

//LOOK plant asset in example does not have name or status which are required in schema

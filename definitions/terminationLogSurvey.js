// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 11, planting_div_veg
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//UUIDs
let terminationLogUUID = randomUUID();
let conventionUUID = randomUUID();
let logCategoryUUID = randomUUID();
let areaPercentageUUID = randomUUID();
let percentageUnitUUID = randomUUID();

//Main entity
let terminationLog = new builder.SchemaOverlay({
    typeAndBundle: 'log--activity',
    name: 'termination',
    validExamples: [terminationLogExample],
    erroredExamples: [terminationLogError]    
});
terminationLog.setMainDescription("Add termination event if an termination date is included in the planting information.");

terminationLog.setConstant({
    attribute:"status",
    value:"done",
    description: "The status should always be set to done to inherit the area."
});

terminationLog.setConstant({
    attribute:"is_termination",
    value:"true",
});

//taxonomy term - log_category
let logCategory = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "termination",
    validExamples: [logCategoryExample],
    erroredExamples: [logCategoryError],
});

//do we need anything for log harvest here? like log category or yield quantity or should that be a separate convention

//Convention
// Object
let terminationConvention = new builder.ConventionSchema({
    title: "Diverse Vegetable Seed Treatment",
    version: "0.0.1",
    schemaName:"log--activity--termination",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`
    ##Purpose\n
    Termination log to be applied to a plant asset only if the farmer indicated the date of termination.\n
    ##Specification\n
    text\n`,
    validExamples: [terminationConventionExample],
    erroredExamples: [terminationConventionError]
});

////add attributes
terminationConvention.addAttribute( { schemaOverlayObject:terminationLog, attributeName: "termination_log", required: true } );
terminationConvention.addAttribute( { schemaOverlayObject:logCategory, attributeName: "log_category", required: true});
terminationConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "plant_asset", required: true } );
terminationConvention.addAttribute( { schemaOverlayObject: "quantity--standard--area_percentage", attributeName: "area_quantity", required: false});
terminationConvention.addAttribute( { schemaOverlayObject: "taxonomy_term--unit--%", attributeName: "area_unit", required: false});

////add relationships
terminationConvention.addRelationship( { containerEntity: "termination_log", relationName:"category", mentionedEntity:"log_category", required: true});
terminationConvention.addRelationship( { containerEntity:"termination_log", relationName:"asset", mentionedEntity:"plant_asset", required: true } );
terminationConvention.addRelationship( { containerEntity:"termination_log" , relationName:"quantity" , mentionedEntity:"area_quantity" , required: false } );
terminationConvention.addRelationship( { containerEntity:"area_quantity" , relationName:"units" , mentionedEntity:"area_unit" , required: false } );

//Examples
var logCategoryExample = {
    id: logCategoryUUID,
    attributes: {
        name: "termination",
    }
}

var logCategoryError = {
    id: logCategoryUUID,
    attributes: {
        name: "done",
    }
}

var terminationLogExample = {
    id: terminationLogUUID,
    attributes: {
        status: "done"
    }
};

var terminationLogError = {
    id: terminationLogUUID,
    attributes: {
        status: "processing"
    }
};

let plantAssetExampleAttributes = terminationConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = terminationConvention.overlays.plant_asset.validExamples[0].id;

var terminationConventionExample = {
        id: conventionUUID,
        type: "Object",
        plant_asset: {
            id: plantAssetUUID,
            attributes: plantAssetExampleAttributes
        },
        termination_log: {
            id: terminationLogUUID,
            attributes: {
                name: "example termination log",
                status: "done",
                is_termination: "true"
            },
            relationships: {
                category: { data: [
                    {
                        type: "taxonomy_term--log_category",
                        id:logCategoryUUID
                    }
                ] },
                asset: { data: [
                    {
                        type: "asset--plant",
                        id: plantAssetUUID
                    },
                ] }
            },
        },
        log_category: logCategoryExample,
        area_quantity: {
            id: areaPercentageUUID,
            attributes: {
                label: "area"
            },
            relationships: {
                units: { data: [
                    {
                        type:"taxonomy_term--unit",
                        id: percentageUnitUUID
                    }
                ] }
            }
        },
        area_unit:  {
            id: percentageUnitUUID,
            attributes: {
                name: "%"
            },   
        },
};

var terminationConventionError = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    termination_log: {
        id: terminationLogUUID,
        attributes: {
            name: "example termination log",
            //status: "done",
            //is_termination: "true"
        },
        relationships: {
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] },
            asset: { data: [
                {
                    type: "asset--plant",
                    id: plantAssetUUID
                },
            ] }
        },
    },
    log_category: logCategoryExample,
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
};

terminationConvention.validExamples = [terminationConventionExample];
terminationConvention.erroredExamples = [terminationConventionError];

let test = terminationConvention.testExamples();
let storageOperation = terminationConvention.store();
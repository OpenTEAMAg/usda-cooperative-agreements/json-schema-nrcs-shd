# tillage_event - v0.0.1

## tillage_log

* **Type: log--activity** 
* **Required: true** 
* Must be related to a taxonomy_term--log_category named *tillage* and be related to an *asset--land*.
* Should have quantity--standard--stir, quantity--standard--residue, quantity--standard--tillage_depth.
* May have other taxonomy_term--log_category.
* Originally hosted in [link](https://gitlab.com/OpenTEAMAg/ag-data-wallet/openteam-convention/-/blob/main/descriptions/log--activity--tillage.md)


 **Overlay Requirements**: 


* name: fixed to tillage.
### stir_quantity

* **Type: quantity--standard** 
* **Required: false** 
* Must be labelled as *stir* and it's measure type is *ratio*.
* See documentation ADD LINK to get the standard specification for this ratio.


 **Overlay Requirements**: 


* measure: fixed to ration.

* label: fixed to STIR.
### depth_quantity

* **Type: quantity--standard** 
* **Required: false** 
* Must be labelled as *depth* and it's measure type is *length*.


 **Overlay Requirements**: 


* measure: fixed to length.

* label: fixed to depth.

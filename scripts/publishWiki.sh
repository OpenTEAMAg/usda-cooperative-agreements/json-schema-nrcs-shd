#!/bin/bash

## $1 is the current branch
cp scripts/python/skeleton/* ./output/documentation
cd scripts/python
if [ $1 = "main" ]; then
    cp mkdocs_main.yml mkdocs.yml
elif [ $1 = "staging" ]; then
    cp mkdocs_staging.yml mkdocs.yml
fi
pip install -r requirements.txt
mkdocs build
cd ../..
if [ $1 = "main" ]; then
    mkdir -p .public/wiki
    mv scripts/python/site/* .public/wiki/
elif [ $1 = "staging" ]; then
    mkdir -p .public/staging_wiki
    mv scripts/python/site/* .public/staging_wiki/
fi
mv .public/* public

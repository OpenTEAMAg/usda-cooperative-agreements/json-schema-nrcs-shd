# Contribution Guidelines

## Git Strategy

  We coordinate our work on this repo by using centralized a branching, merging and testing strategy.
  
### Hard Rules

* You can't commit to `main`. It is a *protected* branch, meaning direct commits won't be accepted into it. The only way to 

### Soft Rules (not enforceable but important)

1. MR requests need to be *small* but *meaningful*. This is a relative concept, so always take the perspective of a reviewer. Some elements need to be together in order to be tested effectively. Otherwise, create different branches and MRs.
2. 

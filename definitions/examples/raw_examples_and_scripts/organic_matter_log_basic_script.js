const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let organicMatterConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--input--organic_matter/object.json` ) );
let organicMatterConvention = new builder.ConventionSchema( organicMatterConventionSrc );

// reading the example
let organicMatterExample = exampleImporter(filename = "organicMatterInputLogSurvey_basic.json");

Object.keys(organicMatterConvention.schema.properties);

// empty object
let formattedExample = {
    organic_matter_log: {},
    log_category: {},
    plant_asset:{},
    area_quantity:{},
    percentage_unit: {},
    moisture: {},
    nitrogen:{},
    phosphorus: {},
    potassium:{},
    rate:{},
    organic_matter_type: {}
};

//the first entity in the convention is the area quantity, assigning that here
formattedExample.area_quantity = organicMatterExample[0];

//second entity is the area unit (%)
formattedExample.area_unit = organicMatterExample[1];

//third entity is the rate
formattedExample.rate = organicMatterExample[2];

//fourth entity is taxonomy_term--material_type
formattedExample.organic_matter_type = organicMatterExample[3];

//fifth entity is the organic matter log
formattedExample.organic_matter_log = organicMatterExample[4];

//sixth entity is plant asset
formattedExample.plant_asset = organicMatterExample[5];

//seventh entity is log category
formattedExample.log_category = organicMatterExample[6];

// testing the example
organicMatterConvention.validExamples = [ formattedExample ];

let test = organicMatterConvention.testExamples();

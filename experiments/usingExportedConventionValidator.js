const validator = require("../output/validators/allConventions.js");
const fs = require("fs");

const schema = JSON.parse( fs.readFileSync("../output/collection/conventions/log--activity--tillage_script/schema.json") );

const example = JSON.parse( fs.readFileSync("../output/collection/conventions/log--activity--tillage_script/examples/correct/example_1.json") );
const error = JSON.parse( fs.readFileSync("../output/collection/conventions/log--activity--tillage_script/examples/incorrect/error_1.json") );

validator["log--activity--tillage_script"](example);
console.log(validator.errors);

validator["log--activity--tillage_script"](error);
console.log(validator["log--activity--tillage_script"].errors);

let error2 = {};
Object.assign(error2, error);
error2.depth_quantity = { attributes: { label: "how deep" } };

validator["log--activity--tillage_script"](error2);
console.log(validator["log--activity--tillage_script"].errors);


let error3 = {};
Object.assign(error3, example);
error3.depth_quantity = { attributes: { label: "depth" } };
error3.tillage_log.relationships.quantity[0].id = 'f8e8bb6c-f25f-4fc7-82de-ded26dc8931f';

validator["log--activity--tillage_script"](error3);
console.log(validator["log--activity--tillage_script"].errors);

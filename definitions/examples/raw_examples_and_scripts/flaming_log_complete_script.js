const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let flamingConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--activity--flaming/object.json` ) );
let flamingConvention = new builder.ConventionSchema( flamingConventionSrc );

// reading the example
let flamingExample = exampleImporter(filename = "flamingLogSurvey_complete.json");

Object.keys(flamingConvention.schema.properties);

// empty object
let formattedExample = {
    flaming_log: {},
    log_category: {},
    area_quantity: {},
    area_unit: {},
    plant_asset:{}
};

//the first entity in the convention is the area quantity, assigning that here
formattedExample.area_quantity = flamingExample[0];

//the second entity in the convention is the area unit
formattedExample.area_unit = flamingExample[1];

//the third entity in the convention is the flaming log
formattedExample.flaming_log = flamingExample[2];

//the fourth entity in the convention is the plant asset
flamingExample[3].attributes.name = "tacit_plant_asset";
flamingExample[3].attributes.status = "active";
formattedExample.plant_asset = flamingExample[3];

//the fifth entity in the convention is the log category
formattedExample.log_category = flamingExample[4];

//sixth entity is also log category, we won't assign this

// testing the example
flamingConvention.validExamples = [ formattedExample ];

let test = flamingConvention.testExamples();

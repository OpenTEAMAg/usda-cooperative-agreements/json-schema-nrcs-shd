{
 "title": "Diverse Vegetable Seed Treatment",
 "type": "object",
 "$id": "www.gitlabrepo.com/version/farmos_conventions/0.0.1/conventions/log--activity--termination",
 "properties": {
  "termination_log": {
   "title": "Activity log",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "log--activity"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "name": {
       "type": "string",
       "title": "Name",
       "maxLength": 255,
       "description": "The name of the log. Leave this blank to automatically generate a name.",
       "default": ""
      },
      "timestamp": {
       "type": "string",
       "title": "Timestamp",
       "format": "date-time",
       "description": "Timestamp of the event being logged."
      },
      "status": {
       "const": "done",
       "description": "The status should always be set to done to inherit the area."
      },
      "is_termination": {
       "const": "true"
      },
      "data": {
       "type": "string",
       "title": "Data"
      },
      "notes": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Text"
        },
        "format": {
         "type": "string",
         "title": "Text format"
        }
       },
       "required": [
        "value"
       ],
       "title": "Notes"
      },
      "flag": {
       "type": "array",
       "title": "Flags",
       "description": "Add flags to enable better sorting and filtering of records.",
       "items": {
        "type": "string",
        "title": "Text value"
       }
      },
      "geometry": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Geometry"
        },
        "geo_type": {
         "type": "string",
         "title": "Geometry Type"
        },
        "lat": {
         "type": "number",
         "title": "Centroid Latitude"
        },
        "lon": {
         "type": "number",
         "title": "Centroid Longitude"
        },
        "left": {
         "type": "number",
         "title": "Left Bounding"
        },
        "top": {
         "type": "number",
         "title": "Top Bounding"
        },
        "right": {
         "type": "number",
         "title": "Right Bounding"
        },
        "bottom": {
         "type": "number",
         "title": "Bottom Bounding"
        },
        "geohash": {
         "type": "string",
         "title": "Geohash"
        },
        "latlon": {
         "type": "string",
         "title": "LatLong Pair"
        }
       },
       "title": "Geometry",
       "description": "Add geometry data to this log to describe where it took place."
      },
      "is_movement": {
       "type": "boolean",
       "title": "Is movement",
       "description": "If this log is a movement, then all assets referenced by it will be located in the referenced locations and/or geometry at the time the log takes place. The log must be complete in order for the movement to take effect."
      },
      "surveystack_id": {
       "type": "string",
       "title": "Surveystack ID",
       "maxLength": 255
      },
      "quick": {
       "type": "array",
       "title": "Quick form",
       "description": "References the quick form that was used to create this record.",
       "items": {
        "type": "string",
        "title": "Text value",
        "maxLength": 255
       }
      }
     },
     "required": [
      "status"
     ],
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "file": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Files",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "image": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Images",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "location": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Location",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "asset": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Assets",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "category": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "minContains": 1,
         "uniqueItems": true,
         "contains": {
          "oneOf": [
           {
            "type": "object",
            "properties": {
             "type": {
              "const": "taxonomy_term--log_category"
             },
             "id": {
              "const": {
               "$data": "/log_category/id"
              }
             }
            }
           }
          ]
         }
        }
       }
      },
      "quantity": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Quantity",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      },
      "owner": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Owners",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      }
     },
     "type": "object",
     "additionalProperties": false
    }
   },
   "description": "Add termination event if an termination date is included in the planting information."
  },
  "log_category": {
   "title": "Log category taxonomy term",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "taxonomy_term--log_category"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "drupal_internal__tid": {
       "type": "integer",
       "title": "Term ID",
       "description": "The term ID."
      },
      "status": {
       "type": "boolean",
       "title": "Published",
       "default": true
      },
      "name": {
       "type": "string",
       "title": "Name",
       "maxLength": 255
      },
      "description": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Text"
        },
        "format": {
         "type": "string",
         "title": "Text format"
        }
       },
       "required": [
        "value"
       ],
       "title": "Description"
      },
      "weight": {
       "type": "integer",
       "title": "Weight",
       "description": "The weight of this term in relation to other terms.",
       "default": 0
      }
     },
     "required": [
      "name"
     ],
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "vid": {
       "type": "object",
       "properties": {
        "data": {
         "title": "Vocabulary",
         "type": "object",
         "required": [
          "id",
          "type"
         ],
         "properties": {
          "id": {
           "type": "string",
           "title": "Resource ID",
           "format": "uuid",
           "maxLength": 128
          },
          "type": {
           "type": "string"
          }
         }
        }
       }
      },
      "parent": {
       "type": "object",
       "properties": {
        "data": {
         "type": "array",
         "title": "Term Parents",
         "items": {
          "type": "object",
          "required": [
           "id",
           "type"
          ],
          "properties": {
           "id": {
            "type": "string",
            "title": "Resource ID",
            "format": "uuid",
            "maxLength": 128
           },
           "type": {
            "type": "string"
           }
          }
         }
        }
       }
      }
     },
     "type": "object",
     "additionalProperties": false
    }
   },
   "description": "The associated log implies planting termination."
  }
 },
 "description": "\n    ##Purpose\n\n    Termination log to be applied to a plant asset only if the farmer indicated the date of termination.\n\n    ##Specification\n\n    text\n",
 "required": [
  "termination_log",
  "log_category"
 ]
}
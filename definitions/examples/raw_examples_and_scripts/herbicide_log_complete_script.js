const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter, findMissingEntitiesInExample } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let herbicideConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--input--herbicide/object.json` ) );
let herbicideConvention = new builder.ConventionSchema( herbicideConventionSrc );

// reading the example
let herbicideExample = exampleImporter(filename = "herbicideLogSurvey_complete.json");

let missingEntities = findMissingEntitiesInExample(herbicideExample);

Object.keys(herbicideConvention.schema.properties);

// empty object
let formattedExample = {
    herbicide_log: {},
    plant_asset: {},
    rate: {},
    rate_unit:{},
    area_quantity:{},
    area_unit:{},
    herbicide_name: {},
    log_category: {}
};

//the first entity in the convention is the area quantity, assigning that here
formattedExample.area_quantity = herbicideExample[0];

//the second entity in the convention is the area unit
formattedExample.area_unit = herbicideExample[1];

//third entity is rate
formattedExample.rate = herbicideExample[2];

//fourth entity is rate unit
formattedExample.rate_unit = herbicideExample[3];

//fifth entity is taxonomy_term--material_type (2_4_d)
formattedExample.herbicide_name = herbicideExample[4];

//sixth entity is also taxonomy_term--material_type (insecticide), do we need to edit convention for this?

//seventh entity is quantity--standard, active_ingredient_percent, need to update convention

//eight entity is taxonomy_term--unit (%) for active ingredient

//ninth entity is the herbicide log
formattedExample.herbicide_log = herbicideExample[8];

//tenth entity is plant asset
// ADDITION
// This plant asset was added by our examples parser, which detected that it was mentioned but not included (probably it comes from another region in the survey).
// We need to fill in all the fields that are mandatory before adding it.

// UNCOMENT HERE
herbicideExample[9].attributes.name = "tacit_plant_asset";
herbicideExample[9].attributes.status = "active";
formattedExample.plant_asset = herbicideExample[9];


//eleventh entity is log category
formattedExample.log_category = herbicideExample[10];

// testing the example
herbicideConvention.validExamples = [ formattedExample ];

let test = herbicideConvention.testExamples();

// COMMENTS from Octavio
// The missing 'asset-plant' was added by the functions I had already provided, what this error is telling you is you need to make it schema compliant. Let's see:
test.failedExamples[0].errors.find( d => d.instancePath.match("plant_asset") );
// It is missing a name. When you add it, you should also fill the empty fields. Apparently, we are only missing the name. Look at "ADDITION", currently in line 55, just after you add the plant asset from the array into the object.
// Uncomet the lines marked with "UNCOMENT HERE" and it will work.

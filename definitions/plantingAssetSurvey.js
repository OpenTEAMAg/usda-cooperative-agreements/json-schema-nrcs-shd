// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 11, planting_div_veg
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

// ids for the examples
let plantAssetUUID = randomUUID();
let seasonUUID = randomUUID();
let speciesUUID = randomUUID();
let varietyUUID = randomUUID();
let conventionUUID = randomUUID();

// // Main entity: Plant Asset
let plantingAssetExample = {
    id:plantAssetUUID,
    attributes: {
        name: "example div veg plant asset",
        geometry: null,
        status:"active"
    }
};

let plantingAssetError = {
    id:plantAssetUUID,
    attributes: {
        name: "example div veg plant asset",
        geometry: "[91.93934434,-40.345345]",
        status:"true"
    }
};

let plantingPlantAsset = new builder.SchemaOverlay({
    typeAndBundle: 'asset--plant',
    name: 'planting',
    validExamples: [plantingAssetExample],
    erroredExamples: [plantingAssetError]    
});

plantingPlantAsset.setMainDescription("The planting is the main asset that all management logs will reference.");
plantingPlantAsset.setConstant({
    attribute:"status",
    value:"active"
});

plantingPlantAsset.setConstant({
    attribute:"geometry",
    value:null
});


// let Taxonomy Terms
let varietyExample = {
    id: varietyUUID,
    attributes: {
        name: "laccinato"
    }
};
let varietyError = {
    id: varietyUUID,
    attributes: {
        label: "laccinato"
    }
};
let variety = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--plant_type",
    name: 'variety',
    validExamples: [varietyExample],
    erroredExamples: [varietyError]
});
variety.setMainDescription("The variety of the vegetable planted.");

let speciesExample = {
    id: speciesUUID,
    attributes: {
        name: "kale"
    }
};
let speciesError = {
    id: speciesUUID,
    attributes: {
        label: "kale"
    }
};
let species = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--plant_type",
    name: "species",
    validExamples: [speciesExample],
    erroredExamples: [speciesError],
});
species.setMainDescription("The species of the vegetable planted.");

let seasonExample = {
    id: seasonUUID,
    attributes: {
        name: "spring, 2022"
    }
};
let seasonError = {
    id: seasonUUID,
    attributes: {
        label: "spring, 2022"
    }
};
let season = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--season",
    name: "season",
    validExamples: [seasonExample],
    erroredExamples: [seasonError]
});
season.setMainDescription("A season taxonomy term should exist for each working period, and logs and assets should be associated to the related seasons. It is up to the user to decide how they want to categorize their seasons - ex: 2016, or Fall, 2022.");


//// use a taxonomy_term--season in the plant asset

// // // Convention
// object
let plantingConvention = new builder.ConventionSchema({
    title: "Diverse Vegetable Planting",
    version: "0.0.1",
    schemaName:"asset--plant--div_veg_planting",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`
    ##Purpose\n
    This is the main entity that represents a cultivar which will be referenced by all subsequent managment logs.\n
    ##Specification\n
    text\n`,
    validExamples: [],
    erroredExamples: []
});

plantingConvention.addAttribute( { schemaOverlayObject:plantingPlantAsset, attributeName: "plant_asset", required: true } );
plantingConvention.addAttribute( { schemaOverlayObject:species, attributeName: "species_taxonomy", required: true } );
plantingConvention.addAttribute( { schemaOverlayObject:variety, attributeName: "variety_taxonomy", required: false } );
plantingConvention.addAttribute( { schemaOverlayObject:season, attributeName: "season_taxonomy", required: true } );

plantingConvention.addRelationship( { containerEntity:"plant_asset" , relationName:"plant_type" , mentionedEntity:"species_taxonomy" , required: true } );
plantingConvention.addRelationship( { containerEntity:"plant_asset" , relationName:"plant_type" , mentionedEntity:"variety_taxonomy" , required: true } );
plantingConvention.addRelationship( { containerEntity:"plant_asset" , relationName:"season" , mentionedEntity:"season_taxonomy" , required: true } );

let plantingConventionExample = {
    id: conventionUUID,
    plant_asset: {
        id:plantAssetUUID,
        attributes: {
            name: "Example div veg plant asset",
            geometry: null,
            status:"active"
        },
        relationships: {
            plant_type: { data: [
                {
                    type: "taxonomy_term--plant_type",
                    id:speciesUUID
                },
                {
                    type: "taxonomy_term--plant_type",
                    id:varietyUUID
                }
            ] },
            season: { data: [
                {
                    type: "taxonomy_term--season",
                    id: seasonUUID
                }
            ] }
        }
    },
    season_taxonomy: seasonExample,
    species_taxonomy: speciesExample,
    variety_taxonomy: varietyExample
};

let plantingConventionError = {
    id: conventionUUID,
    plant_asset: {
        id:plantAssetUUID,
        attributes: {
            name: "Example div veg plant asset",
            geometry: null,
            status:"active"
        },
        relationships: {
            plant_type: { data: [
                {
                    type: "taxonomy_term--plant_type",
                    id:varietyUUID
                }
            ] }
        }
    },
    season_taxonomy: seasonError,
    species_taxonomy: speciesError,
    variety_taxonomy: varietyError
};

plantingConvention.validExamples = [plantingConventionExample];
plantingConvention.erroredExamples = [plantingConventionError];

let test = plantingConvention.testExamples();
let storageOperation = plantingConvention.store();

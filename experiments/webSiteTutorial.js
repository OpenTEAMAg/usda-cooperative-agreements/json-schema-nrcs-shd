// This file  shows an intermediate example in which a mildly complex schema (article) calls properties from another schema and we manage to validate them.
const Ajv = require("ajv");


let locationSchema = {
    "$id": "https://example.com/geographical-location.schema.json",
    // "$schema": "https://json-schema.org/draft/2020-12/schema",
    "title": "Longitude and Latitude",
    "description": "A geographical coordinate on a planet (most commonly Earth).",
    "required": [ "latitude", "longitude" ],
    "type": "object",
    "properties": {
        "latitude": {
            "type": "number",
            "minimum": -90,
            "maximum": 90
        },
        "longitude": {
            "type": "number",
            "minimum": -180,
            "maximum": 180
        }
    }
};

let articleSchema = {
    "$id": "https://example.com/product.schema.json",
  // "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Product",
  "description": "A product from Acme's catalog",
  "type": "object",
  "properties": {
    "productId": {
      "description": "The unique identifier for a product",
      "type": "integer"
    },
    "productName": {
      "description": "Name of the product",
      "type": "string"
    },
    "price": {
      "description": "The price of the product",
      "type": "number",
      "exclusiveMinimum": 0
    },
    "tags": {
      "description": "Tags for the product",
      "type": "array",
      "items": {
        "type": "string"
      },
      "minItems": 1,
      "uniqueItems": true
    },
    "dimensions": {
      "type": "object",
      "properties": {
        "length": {
          "type": "number"
        },
        "width": {
          "type": "number"
        },
        "height": {
          "type": "number"
        }
      },
      "required": [ "length", "width", "height" ]
    },
    "warehouseLocation": {
      "description": "Coordinates of the warehouse where the product is located.",
      "$ref": "https://example.com/geographical-location.schema.json"
    }
  },
  "required": [ "productId", "productName", "price" ]
};

let exampleArticle = {
    "productId": 1,
    "productName": "An ice sculpture",
    "price": 12.50,
    "tags": [ "cold", "ice" ],
    "dimensions": {
        "length": 7.0,
        "width": 12.0,
        "height": 9.5
    },
    "warehouseLocation": {
        "latitude": -78.75,
        "longitude": 20.4
    }
};


let ajv = new Ajv({allErrors:true}); 
ajv.addSchema(locationSchema);
let validate = ajv.compile(articleSchema);

let valid = validate(exampleArticle);
if (!valid) console.log(validate.errors);


let errorArticle = {
    "productId": "1",
    "productName": "An ice sculpture",
    "tags": [ "cold", "ice" ],
    "dimensions": {
        "length": 7.0,
        "width": 12.0,
        "height": 9.5
    },
    "warehouseLocation": {
        "latitude": -78.75,
        "longitude": "20.4"
    }
};

let error = validate(errorArticle);
if (!error) console.log(validate.errors);

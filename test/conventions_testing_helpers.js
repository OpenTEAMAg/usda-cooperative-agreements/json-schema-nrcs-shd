const fs = require('fs');
const {buildValidator} = require('../src/schema_utilities.js');

/**
 * @param {string} convention -- Convenion title.
 * @param {string} mainPath -- The leading from this file's location (`./test` from the root of the repo) to a folder in which all your collection outputs are stored, inside which a folder for each convention will exists, accordint to the structure automatically used by our convention builder.
 */
function testConventionExamples({ convention, mainPath = "../output/collection/conventions" }) {
    describe( `Convention: ${convention}`, () => {
        let correctExamples = fs.readdirSync( `${__dirname}/${mainPath}/${convention}/examples/correct`, (error, files) => {
            return files;
        } );
        let incorrectExamples  = fs.readdirSync( `${__dirname}/${mainPath}/${convention}/examples/incorrect`, (error, files) => {
            return files;
        } );

        let conventionSchema = JSON.parse( fs.readFileSync( `${__dirname}/${mainPath}/${convention}/schema.json` ) );

        // We need to detect which schemas are needed for the subsidiary entities and feed them to the validator.
        let requiredSchemas = Object.keys( conventionSchema.properties )
            .map( subEntity => conventionSchema.properties[subEntity].$ref )
            .filter( d => d )
            .map( url => url.replace("https://ourscitest.farmos.net/api/", "").replace(/\/resource.*$/, "").replace("/", "--") )
        ;

        requiredSchemas = Array.from( new Set(requiredSchemas) );

        let generalValidator = buildValidator();

        requiredSchemas.forEach( schemaString => {
            let type = schemaString.split("--")[0];
            let bundle = schemaString.split("--")[1];
            let subEntitySchema = farmosSchemata[type][bundle];
            delete subEntitySchema.$schema;
            generalValidator.addSchema(subEntitySchema);
        } );

        let validator = generalValidator.compile(conventionSchema);

        if ( correctExamples.length == 0 ) {
            throw `No correct examples have been provided for the convention ${convention}. Please, add some examples of entities with the desired format, so we can be sure they are approved by the schema.`;
        };

        correctExamples.forEach( correct => {
            let testData = JSON.parse( fs.readFileSync(`${__dirname}/${mainPath}/${convention}/examples/correct/${correct}`) );

            test( `Correct case described by file ${correct} for convention ${convention}`, () => {
                let validation = validator(testData);
                if (!validation) {
                    console.log("Observed errors");
                    console.log(validator.errors);
                }
                expect(validation).toBe(true);
            } );
        } );

        if ( incorrectExamples.length == 0 ) {
            throw `No incorrect examples have been provided for the convention ${convention}. Checking the schema is able to reject wrong entities is as important as checking it approves the right ones. Please, add some examples of failed entities.`;
        };

        incorrectExamples.forEach( incorrect => {
            let testData = JSON.parse( fs.readFileSync(`${__dirname}/${mainPath}/${convention}/examples/incorrect/${incorrect}`) );

            test( `Incorrect case described by file ${incorrect} for convention ${convention}`, () => {
                let validation = validator(testData);
                expect(validation).toBe(false);
            } );
        } );
    } );
};

exports.testConventionExamples = testConventionExamples;

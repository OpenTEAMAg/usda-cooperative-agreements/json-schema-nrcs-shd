// In this example, we will try to transform a convention in natural text, assuming it is well structured enough and it has comments which are compliant with the suggested format.
// This link seems to contain a reasonable way to use JSDoc to turn the MarkDown into HTML. Clearly a wiki is the best option. https://stackoverflow.com/questions/61071898/jsdoc-how-to-include-multiple-md-file
const fs = require("fs");
const {urlToStringPair} = require("../src/schema_utilities.js");

let conventionName = "log--activity--tillage";


let convention = JSON.parse( fs.readFileSync(`${__dirname}/../conventions/log--activity--tillage/schema.json`) );

let text = `# ${ conventionName }\n\n`;


// We need to structure the tree of relationships first, then start with the root of the tree and add subtitles for each branch.

let subEntities = Object.keys( convention.properties );

subEntities.forEach( subEntity => {
    let entity = convention.properties[subEntity];
    text = text.concat(`## ${subEntity}`, "\n\n", `* **Type: ${urlToStringPair( entity.$ref )}** \n` ,`  * ${entity.description.replace( /\.\s/g, ".\n  * " )}`, "\n\n");
} );

fs.writeFileSync( "./exampleDescriptionText.md", text, console.error );

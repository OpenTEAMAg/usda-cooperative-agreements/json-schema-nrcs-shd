// This test loads all conventions and their associated examples and ensures examples result as stated, thus validating the final convention outputs.
const fs = require('fs');
const {testConventionExamples} = require('./conventions_testing_helpers.js');

let farmosSchemata = JSON.parse( fs.readFileSync(`${ __dirname }/../input/farmos.json`) );
let hyperSchema = JSON.parse( fs.readFileSync(`${ __dirname }/../src/hyper-schema.json`) );

// We wil obtain the names of all conventions, which are the first level folders here, and test for each one agains all examples

let conventions = fs.readdirSync( `${__dirname}/../output/collection/conventions`, (error, files) => {
    return files;
} );

conventions.forEach( convention => testConventionExamples({ convention: convention, mainPath: "../output/collection/conventions" }) );

// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 40, irrigation
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//UUIDS
let irrigationLogUUID = randomUUID();
let logCategoryUUID = randomUUID();
let conventionUUID = randomUUID();
let totalWaterUUID = randomUUID();
let waterUnitUUID = randomUUID();
let effectivenessUnitUUID = randomUUID();
let effectivenessUUID = randomUUID();
let areaPercentageUUID = randomUUID();
let percentageUnitUUID = randomUUID();

//Main entity
////Examples
let irrigationLogExample = {
    id:irrigationLogUUID,
    attributes: {
        name: "irrigation log",
        status:"done",
    }
};
let irrigationLogError = {
    id:irrigationLogUUID,
    attributes: {
        name: "irrigation log",
        status:"pending",
    }
};
////overlays and constants
let irrigationLog = new builder.SchemaOverlay({
    typeAndBundle: 'log--input',
    name: 'irrigation',
    validExamples: [irrigationLogExample],
    erroredExamples: [irrigationLogError]    
});
//LOOK add description
irrigationLog.setMainDescription(
    "Irrigation logs are used to record watering events. It records % of field covered, water source, quantity, and general effectivness."
    )

irrigationLog.setConstant({
    attribute:"status",
    value:"done"
});

//taxonomy term - log_category
////Examples
let logCategoryExample = {
    id: logCategoryUUID,
    attributes: {
        name: "irrigation"
    }
};
let logCategoryError = {
    id: logCategoryUUID,
    attributes: {
        name: "activity"
    }
};
////overlays and constants
let logCategory = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "irrigation",
    validExamples: [logCategoryExample],
    erroredExamples: [logCategoryError],
});

//quantities - total water
////Examples
let totalWaterExample = {
    id: totalWaterUUID,
    attributes: {
        name: "total water",
        measure:"value",
    }
};
let totalWaterError = {
    id: totalWaterUUID,
    attributes: {
        name: "total water",
        measure:"volume",
    }
};
////Overlays and constants
let totalWater = new builder.SchemaOverlay({
    typeAndBundle: "quantity--material",
    name: "total_water",
    validExamples: [ totalWaterExample ],
    erroredExamples: [ totalWaterError ]
});
//LOOK set description
totalWater.setMainDescription("This represents the total quantity of water added.");
//LOOK what should the measure be here?
totalWater.setConstant({
    attribute: "measure",
    value:"value"
});
//totalWater.setConstant({
//    attribute: "label",
//    value:"total_water"
//});

//taxonomy term - units
let waterUnit = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "inches"
});
waterUnit.setMainDescription("Units for total water added are recorded in inches.");

//quantity - effectiveness
////Examples
let effectivenessExample = {
    id: effectivenessUUID,
    attributes: {
        name: "effectiveness"
    }
};
let effectivenessError = {
    id: effectivenessUUID,
    attributes: {
        name: "activity"
    }
};
////overlays and examples
let effectiveness = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "effectiveness",
    validExamples: [ effectivenessExample ],
    erroredExamples: [ effectivenessError ]
});
effectiveness.setConstant({
    attribute: "label",
    value:"effectiveness"
});
//LOOK set description - Adie
effectiveness.setMainDescription("Estimate if the field was under watered, mostly watered, or well watered.");

//Taxonomy term -- unit
////Examples
let effectivenessUnitExample = {
    id: effectivenessUnitUUID,
    attributes: {
        name: "Under watered (0)"
    }
};
let effectivenessUnitError = {
    id: effectivenessUnitUUID,
    attributes: {
        name: "good amount"
    }
};
////overlays and constants
let effectivenessUnit = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "effectiveness rating",
    validExamples: [ effectivenessUnitExample ],
    erroredExamples: [ effectivenessUnitError ]
});
effectivenessUnit.setEnum({
    attribute: "name",
    valuesArray: [
        "Under watered (0)",
        "Mostly watered (1)",
        "Well watered (2)",
    ],
    description: "This is an effectiveness scale with three levels from under watered to well watered."
});
//LOOK set description - Adie
effectivenessUnit.setMainDescription("This question does not use measured units, this uses a subjective scale where the user will have to use the knowledge of their operation to judge how well the field was watered.")


//convention
let irrigationConvention = new builder.ConventionSchema({
    title: "Irrigation",
    version: "0.0.1",
    schemaName:"log--input--irrigation",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    //LOOK add description
    description:`
    ##Purpose\n
    Irrigation logs are used to record watering events.\n
    ##Specification\n
    text\n`,
    validExamples: [],
    erroredExamples: []
});

irrigationConvention.addAttribute( { schemaOverlayObject:irrigationLog, attributeName: "irrigation_log", required: true } );
irrigationConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "plant_asset", required: true } );
irrigationConvention.addAttribute({ schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "area_quantity", required: false});
irrigationConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "percentage_unit", required: false});
irrigationConvention.addAttribute({ schemaOverlayObject:logCategory, attributeName: "log_category", required: false});
irrigationConvention.addAttribute({ schemaOverlayObject:totalWater, attributeName: "total_water", required: false});
irrigationConvention.addAttribute({ schemaOverlayObject:waterUnit, attributeName: "water_unit", required: false});
irrigationConvention.addAttribute({ schemaOverlayObject:effectiveness, attributeName: "effectiveness", required: false});
irrigationConvention.addAttribute({ schemaOverlayObject:effectivenessUnit, attributeName: "effectiveness_unit", required: false});

irrigationConvention.addRelationship( { containerEntity:"irrigation_log", relationName:"asset", mentionedEntity:"plant_asset", required: true } );
irrigationConvention.addRelationship( { containerEntity:"irrigation_log", relationName:"category", mentionedEntity:"log_category", required: true } );
irrigationConvention.addRelationship( { containerEntity:"irrigation_log" , relationName:"quantity" , mentionedEntity:"area_quantity" , required: false } );
irrigationConvention.addRelationship( { containerEntity:"area_quantity" , relationName:"units" , mentionedEntity:"percentage_unit" , required: false } );
irrigationConvention.addRelationship( { containerEntity:"irrigation_log" , relationName:"quantity" , mentionedEntity:"total_water" , required: false } );
irrigationConvention.addRelationship( { containerEntity:"total_water" , relationName:"units" , mentionedEntity:"water_unit" , required: false } );
irrigationConvention.addRelationship( { containerEntity:"irrigation_log" , relationName:"quantity" , mentionedEntity:"effectiveness" , required: false } );
irrigationConvention.addRelationship( { containerEntity:"effectiveness" , relationName:"units" , mentionedEntity:"effectiveness_unit" , required: false } );

let plantAssetExampleAttributes = irrigationConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = irrigationConvention.overlays.plant_asset.validExamples[0].id;

// Convention example
let irrigationConventionExample = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    irrigation_log: {
        id: irrigationLogUUID,
        attributes: {
            name: "example irrigation log",
            status: "done",
        },
        relationships: {
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] },
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                },
                {
                    type: "quantity--material",
                    id: totalWaterUUID
                },
                {
                    type: "quantity--standard",
                    id: effectivenessUUID
                }
            ] }
        },
    },
    log_category: logCategoryExample,
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
    total_water: {
        id: totalWaterUUID,
        attributes: {
            label: "total_water"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: waterUnitUUID
                }
            ] }
        }
    },
    water_unit:  {
        id: waterUnitUUID,
        attributes: {
            name: "inches"
        },   
    },
    effectiveness: {
        id: effectivenessUUID,
        attributes: {
            label: "effectiveness"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: effectivenessUnitUUID
                }
            ] }
        }
    },
    effectiveness_unit:  {
        id: effectivenessUnitUUID,
        attributes: {
            name: "Under watered (0)"
        },   
    }
};

let irrigationConventionError = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    irrigation_log: {
        id: irrigationLogUUID,
        attributes: {
            name: "example irrigation log",
            status: "done",
        },
        relationships: {
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] },
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                },
                /*{
                    type: "quantity--material",
                    id: totalWaterUUID
                },*/
                {
                    type: "quantity--standard",
                    id: effectivenessUUID
                }
            ] }
        },
    },
    log_category: logCategoryExample,
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
    total_water: {
        id: totalWaterUUID,
        attributes: {
            label: "total_water"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: waterUnitUUID
                }
            ] }
        }
    },
    water_unit:  {
        id: waterUnitUUID,
        attributes: {
            name: "inches"
        },   
    },
};

irrigationConvention.validExamples = [irrigationConventionExample];
irrigationConvention.erroredExamples = [irrigationConventionError];

let test = irrigationConvention.testExamples();
let storageOperation = irrigationConvention.store();
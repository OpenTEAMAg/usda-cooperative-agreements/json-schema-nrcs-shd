# What's stored in this folder

  In this folder, we have *definition* files, akin to the ones we stored on the main `definitions` folder in this repo.
  This one's are not meant for publication, but to test or research certain properties of JSON schema and our convention builder, or even as examples.

# Mechanism

* A script called `test_specific_conventions_against_examples.js` will grab each convention created inside `test/testing_conventions_definitions` and run it's schema against it's associated valid and errored examples, *wtihout using the builder library*, to ensure maximum validity of the test.


# Intention

* To create **very simple**, immediate conventions **highlighting** a unique feature.
* Through providing `validExamples` and `errorExamples` and leveraging our convention auto test feature, you will get many test cases from each. Teste every meaningful and relevant situation through different examples.

# Procedure

* Create your *example conventions*  inside this folder.
* **Remember** to point the storage to the `tests` root by using the `storageFolder` parameter, like this.

```js
let testConvention = new builder.ConventionSchema({
    .
    .
    .
    storageFolder: `test/testing_conventions_definitions`
});
```

* Try to be **very** specific to what you intend to test. Create an on purpose really **bare** convention, with as little entities as needed to show your test case and nothing else.

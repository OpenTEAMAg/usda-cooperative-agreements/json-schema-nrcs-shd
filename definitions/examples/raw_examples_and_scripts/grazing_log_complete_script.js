const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let grazingConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--activity--grazing/object.json` ) );
let grazingConvention = new builder.ConventionSchema( grazingConventionSrc );

// reading the example
let grazingExample = exampleImporter(filename = "grazingLogSurvey_complete.json");

Object.keys(grazingConvention.schema.properties);

// empty object
let formattedExample = {
    grazing_log: {},
    log_category: {},
    animal_count: {},
    animal_weight: {},
    weight_unit:{},
    subpaddocks:{},
    area_quantity:{},
    area_unit:{},
    plant_asset: {}
};

//the 1st entity in the convention is the area quantity, assigning that here
formattedExample.area_quantity = grazingExample[0];

//2nd entity is the area unit (%)
formattedExample.area_unit = grazingExample[1];

//3rd entity is quantity--standard, head
formattedExample.animal_count = grazingExample[2];

//4th entity is quantity--standard, weight
formattedExample.animal_weight = grazingExample[3];

//5th entity is taxonomy_term--unit, LOOK name is 'a' right now, I think this is a placeholder for weight
formattedExample.weight_unit = grazingExample[4];

//6th entity is quantity--standard, subsections
formattedExample.subpaddocks = grazingExample[5];

//7th entity is log--activity
formattedExample.grazing_log = grazingExample[6];

//7th entity is log--activity
formattedExample.grazing_log = grazingExample[6];

//8th entity is the plant asset
grazingExample[7].attributes.name = "tacit_plant_asset";
grazingExample[7].attributes.status = "active";
formattedExample.plant_asset = grazingExample[7];

//9th entity is taxonomy_term--log_category
formattedExample.log_category = grazingExample[8];

//10th entity is also taxonomy_term--log_category

//then entities repeat

// testing the example
grazingConvention.validExamples = [ formattedExample ];

let test = grazingConvention.testExamples();

// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 11, planting_div_veg
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//UUIDs
let seedlingTreatementLogUUID = randomUUID();
let logCategoryUUID = randomUUID();
let conventionUUID = randomUUID();
let areaPercentageUUID = randomUUID();
let percentageUnitUUID = randomUUID();

//Main entity
let seedlingTreatmentLog = new builder.SchemaOverlay({
    typeAndBundle: 'log--input',
    name: 'seedling_treatment',
    validExamples: [seedlingTreatmentLogExample],
    erroredExamples: [seedlingTreatmentLogError]    
});
seedlingTreatmentLog.setMainDescription("This adds a seedling treatment that records the application of fungicide, insecticide, or a combination of both if one is indicated as used in the planting information.");

seedlingTreatmentLog.setConstant({
    attribute:"status",
    value:"done",
    description: "The status should always be set to done to inherit the area."
});

//taxonomy term - log_category
//LOOK is this how you add multiple log categories?
let logCategory = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: ["amendment", "seeding"],
    validExamples: [logCategoryExample],
    erroredExamples: [logCategoryError],
});

//quantity - material (LOOK I don't see a quantity in the survey?)

//Convention
let seedlingTreatmentConvention = new builder.ConventionSchema({
    title: "Diverse Vegetable Seedling Treatment",
    version: "0.0.1",
    schemaName:"log--input--seedling_treatment",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`
    ##Purpose\n
    A seedling treatment log is applied to a plant asset only if the farmer selected one.\n
    ##Specification\n
    text\n`,
    validExamples: [seedlingTreatmentConventionExample],
    erroredExamples: [seedlingTreatmentConventionError]
});

////add attributes
seedlingTreatmentConvention.addAttribute( { schemaOverlayObject:seedlingTreatmentLog, attributeName: "seedling_treatment_log", required: true } );
seedlingTreatmentConvention.addAttribute( { schemaOverlayObject:logCategory, attributeName: "log_category", required: true } );
seedlingTreatmentConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "plant_asset", required: true } );

////add relationships
seedlingTreatmentConvention.addRelationship({ containerEntity: "seedling_treatment_log", relationName:"category", mentionedEntity:"log_category", required: true});
seedlingTreatmentConvention.addRelationship( { containerEntity:"seedling_treatment_log", relationName:"asset", mentionedEntity:"plant_asset", required: true } );

//Examples
var seedlingTreatmentLogExample = {
    id: seedlingTreatementLogUUID,
    attributes: {
        status: "done"
    }
};
var seedlingTreatmentLogError = {
    id: seedlingTreatementLogUUID,
    attributes: {
        status: "true"
    }
};

var logCategoryExample = {
    id: logCategoryUUID,
    attributes: {
        name: "amendment"
    }
};
var logCategoryError = {
    id: logCategoryUUID,
    attributes: {
        name: "irrigation"
    }
};

let plantAssetExampleAttributes = seedlingTreatmentConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = seedlingTreatmentConvention.overlays.plant_asset.validExamples[0].id;

var seedlingTreatmentConventionExample = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    seedling_treatment_log: {
        id: seedlingTreatementLogUUID,
        attributes: {
            name: "example seedling treatment log",
            status: "done",
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: plantAssetUUID
                }
            ]},
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] }
        },
    },
    log_category: logCategoryExample,
    area_quantity: {
       id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
};

var seedlingTreatmentConventionError = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    seedling_treatment_log: {
        id: seedlingTreatementLogUUID,
        attributes: {
            name: "example seedling treatment log",
            status: "pending",
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: plantAssetUUID
                }
            ]},
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] }
        },
    },
    log_category: logCategoryExample,
//    area_quantity: {
//        id: areaPercentageUUID,
//        attributes: {
//            label: "area"
//        },
//        relationships: {
//            units: { data: [
//                {
//                    type:"taxonomy_term--unit",
//                    id: percentageUnitUUID
//                }
//            ] }
//        }
//    },
//    area_unit:  {
//        id: percentageUnitUUID,
//        attributes: {
//            name: "%"
//        },   
//    },
};

seedlingTreatmentConvention.validExamples = [seedlingTreatmentConventionExample];
seedlingTreatmentConvention.erroredExamples = [seedlingTreatmentConventionError];

let test = seedlingTreatmentConvention.testExamples();
let storageOperation = seedlingTreatmentConvention.store();
// Shows how to import an example with the specialized tool we've built and test on it in the `validExamples` field of a ConventionSchema.
// Example relies on a previously written ConventionSchema, the one for Seeding Log, from the Management Survey.
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../src/schema_utilities.js");
const fs = require('fs');

// Let's obtain the preexisting convention, already written. You can read how it is build either by looking into this same object, or by going to it's definition in the source directory `/definitions/seedingLogSurvey.js`.
let seedingConventionSrc = JSON.parse( fs.readFileSync( `./../output/collection/conventions/log--seeding--div_veg_planting/object.json` ) );
let seedingConvention = new builder.ConventionSchema( seedingConventionSrc );


// reading the example as it is provided by SurveyStack using this helper should provide us with an array containning each entity as a discrete resource.
let seedingExample = exampleImporter(filename = "div_veg_planting_basic.json");
// print the result in the terminal
seedingExample;

// Each entity shown here shoudl correspond to an attribute in our schema
// This is the schema
seedingConvention.schema;
// The "properties" contains the places in which we should position the chunks obtained from the example.
seedingConvention.schema.properties;
// If you want to see a clearer "map" you can ask just for the "keys", that is the name of each attribute

Object.keys(seedingConvention.schema.properties);


// Let's create an object with this attribute names and populate it grabbin what's in our example, one entity at a time.
// We stat with an empty object for each attribute, we will replace them one by one.

let formattedExample = {
    seeding_log: {},
    log_category: {},
    area_quantity: {},
    area_unit: {},
    seeding_rate_quantity: {},
    rate_unit: {},
    bed_width_quantity: {},
    width_unit: {},
    plant_asset:{}
};

// Let's look at the first entity in the example
seedingExample[0];
// particularly, the type is "asset--plant"
seedingExample[0].type;
// There's only one attribute with this type in the schema
seedingConvention.schema.properties.plant_asset;
seedingConvention.schema.properties.plant_asset.type;

// This will be our first assigned field
formattedExample.plant_asset = seedingExample[0];

// You can check on the look of our 'formattedExample' now
formattedExample;

// Let's check the following one:
seedingExample[1].type;
// this one is missing from our schema, which implies we need to go back to where 'div_veg_planting_basic' was defined and add the overlay/field/relationships before finishing this. We will ignore it for now.


// Third one is a 'season taxonomy'. Again, missing.
seedingExample[2].type;


// This one is a quantity.
seedingExample[3].type;
//  As there are several candidate fields in the current schema, we need to look into its details.
seedingExample[3].attributes;
// In this case, the "label" is the key.
seedingExample[3].attributes.label;
// let's assign this one into the example object.
formattedExample.area_quantity = seedingExample[3];

// Next entity in the array we obtained from the example is a unit, named "%", we know where to place this one.
seedingExample[4].type;
seedingExample[4].attributes.name;
formattedExample.area_unit = seedingExample[4];

// Next one is a quantity labelled "bed_width"
seedingExample[5].type;
seedingExample[5].attributes.label;
// assign it inside the formatted example
formattedExample.bed_width_quantity = seedingExample[5];

// next, we check that the following entity is its unit by looking into the name
seedingExample[6].type;
seedingExample[6].attributes.name;
formattedExample.width_unit = seedingExample[6];

// next is the seeding rate quantity, with its unit
seedingExample[7].type;
seedingExample[7].attributes.label;

seedingExample[8].type;
seedingExample[8].attributes.name;

formattedExample.seeding_rate_quantity = seedingExample[7];
formattedExample.rate_unit = seedingExample[8];

// Finally, we deal with the seeding log itself
seedingExample[9].type;
formattedExample.seeding_log = seedingExample[9];

// The last entity in the example is this log category
seedingExample[10].type;
formattedExample.log_category = seedingExample[10];


//
// Using the example
//


// Let's insert this example between the valid examples in our schema convention and see if it passes the tests.
seedingConvention.validExamples = [ formattedExample ];

let testResults = seedingConvention.testExamples();
testResults.failedExamples[0].errors;
testResults.failedExamples[0].errors.length;
testResults.failedExamples[0].errors[0];

// fixing value of 'is_movement'

formattedExample.seeding_log.attributes.is_movement;

// Conclussion:
// TODO
// In our schema, 'is_movemennt' is fixed as 'false' with a setConstant command. The survey is creating a 'true' value. This needs to be checked with Greg. Let's change our example for now, so we can keep looking into the examples.

formattedExample.seeding_log.attributes.is_movement = 'false';
// assign the new version of the example and test
seedingConvention.validExamples = [ formattedExample ];

// check again
testResults = seedingConvention.testExamples();
testResults.failedExamples[0].errors.length;
// there's less erros now, meaning this is fixed


// Next error
testResults = seedingConvention.testExamples();
testResults.failedExamples[0].errors.length;
testResults.failedExamples[0].errors[0];

seedingConvention.relationships.filter( rel => rel.containerEntity == "seeding_log" );
formattedExample.seeding_log.relationships.quantity.data;

// add missing relationships to quantities
seedingConvention.addRelationship( { containerEntity:"seeding_log" , relationName:"quantity" , mentionedEntity:"bed_width_quantity" , required: true } );
seedingConvention.addRelationship( { containerEntity:"seeding_log" , relationName:"quantity" , mentionedEntity:"seeding_rate_quantity" , required: true } );

// check again
testResults = seedingConvention.testExamples();
testResults.failedExamples[0].errors.length;

// check another error
testResults.failedExamples[0].errors[0];
formattedExample.seeding_rate_quantity.attributes.label;
// TODO
// the error is in the schema, but let's change the example to keep going
formattedExample.seeding_rate_quantity.attributes.label = "seeding rate";


seedingConvention.validExamples = [ formattedExample ];


// check again
testResults = seedingConvention.testExamples();
testResults.failedExamples[0].errors.length;

// check another error
testResults.failedExamples[0].errors[0];

// TODO
// the error is in the schema, but let's change the example to keep going
formattedExample.bed_width_quantity.attributes.label = "bed width";


seedingConvention.validExamples = [ formattedExample ];


// check again
testResults = seedingConvention.testExamples();
// if we try to read errors again, we get an error:
testResults.failedExamples[0].errors;
// Because the test succeeded this time:
testResults.success;

// Once it succeeds, we would typically store the polished, ready to use example in the "/definitions/examples/processed/" folder using the name of the convention it represents and a number, in this case "log--seeding--div_veg_1.js".
// Since we are not in the appropriate folder, this example also exists in the "/definitions/examples/raw_examples_and_scripts/" folder, in which it has the missing lines needed to store it. Please have a look at it.


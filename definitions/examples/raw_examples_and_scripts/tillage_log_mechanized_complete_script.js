const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let tillageConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--activity--tillage/object.json` ) );
let tillageConvention = new builder.ConventionSchema( tillageConventionSrc );

// reading the example
let tillageExample = exampleImporter(filename = "tillageLogSurvey_mechanized_complete.json");

Object.keys(tillageConvention.schema.properties);

// empty object
let formattedExample = {
    tillage_log: {},
    stir_quantity: {},
    depth_quantity:{},
    area_quantity:{},
    area_unit: {},
    plant_asset: {},
    log_category: {}
};

//the 1st entity in the convention is quantity--standard, area
formattedExample.area_quantity = tillageExample[0];

//the 2nd entity in the convention is the taxonomy_term--unit, %
formattedExample.area_unit = tillageExample[1];

//the 3rd entity in the convention is quantity--standard, depth
formattedExample.depth_quantity = tillageExample[2];

//the 4th entity in the convention is taxonomy_term--unit, in - missing?

//the 5th entity in the convention is quantity--standard, speed - missing

//the 6th entity is taxonomy_term--unit, mph - missing

//7th entity is quantity--standard, stir
formattedExample.stir_quantity = tillageExample[6];

//8th entity is log--activity
formattedExample.tillage_log = tillageExample[7];

//9th entity is asset--plant
formattedExample.plant_asset = tillageExample[8];

//10th entity is taxonomy_term--log_category
formattedExample.log_category = tillageExample[9];

//11th entity is also log_category

//12th entity is asset--equipment



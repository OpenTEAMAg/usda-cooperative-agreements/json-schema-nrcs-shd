const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let seedTreatmentConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--input--seed_treatment/object.json` ) );
let seedTreatmentConvention = new builder.ConventionSchema( seedTreatmentConventionSrc );

// reading the example
let seedTreatmentExample = exampleImporter(filename = "seedTreatmentLogSurvey_basic.json");

Object.keys(seedTreatmentConvention.schema.properties);

// empty object
let formattedExample = {
    seed_treatment_log: {},
    plant_asset: {},
    log_category:{}
};

//the first entity in the convention is the plant asset, assigning that here
formattedExample.plant_asset = seedTreatmentExample[0];

//second entity is taxonomy_term--plant_type, this is missing from convention

//third entity is taxonomy_term--season, this is missing from convention

//fourth entity is log--seeding, this is missing from convention

//fifth entity is log category
formattedExample.log_category = seedTreatmentExample[4];

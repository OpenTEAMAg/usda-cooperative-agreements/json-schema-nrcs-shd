// Tools needed to work with FarmOS drupal native schemas using AJV
const Ajv = require("ajv/dist/2020.js");
const addFormats = require('ajv-formats');
const standaloneCode = require("ajv/dist/standalone").default;
const fs = require('fs');
const { randomUUID } = require('crypto');

/**
 * Apparently, FarmOS.js simplifies `{relationships:  {category: {data: []}}}` to `{relationships:  {category:  []}}`. This function reinstates the intermediate `data` field in a schema to make it upstream compliant again. Used when storing de drupalized schemas.
 * @param {} schema
 */
function fixRelationshipDataField(schema) {
    if (schema.properties.relationships) {
        Object.keys( schema.properties.relationships.properties ).forEach( relKey => {
            let hasData = schema.properties.relationships.properties[relKey]?.properties?.data;
            if (!hasData) {
                schema.properties.relationships.properties[relKey] = {
                    type: "object",
                    properties: {
                        data: schema.properties.relationships.properties[relKey]
                    }
                };
            }
        } );
    }
};

/**
 * Since FarmOS.js returns a monolithic object (which we call `schemataTree` in this documentation), we will store discrete schemas in a tree-like structure of folders, witha  depth level for type and another one for bundle.
 * @param {schemataTree} schemataTree --  De Drupalized Schemas arranged in a tree branched by hierarchy, as FarmOS.js returns.
 */
function storeDeDrupalizedSchemas(schemataTree) {

    fs.mkdirSync( "input/collection", { recursive: true }, console.error );
    Object.keys(schemataTree).forEach( typeKey => {
        Object.keys(schemataTree[typeKey]).forEach( bundleKey => {
            let schema = schemataTree[typeKey][bundleKey];
            delete schema.$schema;
            // add back the 'data' field level inside each relationship property, which is removed by FarmOS.js
            fixRelationshipDataField(schema);
            fs.mkdirSync( `${__dirname}/../input/collection/${typeKey}/${bundleKey}/`, { recursive: true }, console.error );
            let path = `${__dirname}/../input/collection/${typeKey}/${bundleKey}/schema.json`;
            fs.writeFileSync(path, JSON.stringify( schema, null," " ));
            console.log(`writing ${typeKey}--${bundleKey}`);
        } );
    } );
};


/**
 * Builds an AJV object, already parametrized as FarmOS schemas require (this is: it effectively deals with the biolerplate).
 * @param {Object} extraAJVArguments -- Allows to directly send parameters to the new Ajv command. We typically use it to activate the `source` attribute, required to compile static validators.
 * @returns {Ajv}
 */
function buildValidator(extraAJVArguments = false) {
    let ajvArguments = {
        // don't know if needed, test
        allowUnionTypes: true,
        // needed to get all the errors at once, we do not only need to reject a schema but also provide enought information to fix the entity
        allErrors: true,
        // needed to use internal data constraints in conventions, currently to validate relationships.
        $data:true,
        // needed to compile static validators
        // TODO remove, moved to thenew extraAJVArguments when needed
        // code: {source: true}
        strict: false
    };
    if (extraAJVArguments) {
        Object.keys(extraAJVArguments).forEach( key => { ajvArguments[key] = extraAJVArguments[key]; } );
    }
    let validator = new Ajv(ajvArguments);
    // TODO
    // apparently, the only format we need is date-time, so this function should be replaced with the schema
    addFormats(validator);

    // validator
    // draft6 schema is the JSON schema project official schema for a concise year/set of features (let's say 2019, this is not the newest one).
    // .addSchema(draft6Schema)
    ;
    return validator;
};


/**
 * Provided with a type/bundle pair and a source of schemata in which both exist, it will initialize an Ajv object, create a standalone/static validator and store it in the adequate directory (with one level for type, another for bundle).
 * @param {string} type -- A FarmOS type known to the `schemataTree` provided.
 * @param {string} bundle -- A FarmOS bundle known to the `schemataTree` provided.
 * @param {schemataTree} schemataTree -- De Drupalized Schemas arranged in a tree branched by hierarchy, as FarmOS.js returns.
 * @returns {} 
 */
function buildStaticValidator(type, bundle, schemataTree) {
    let validatorObj = buildValidator( { code: { source: true } } );
    let schema = schemataTree[type][bundle];
    delete schema.$schema;
    let validator = validatorObj.compile(schema);
    let moduleCode = standaloneCode(validatorObj, validator);

    // Now you can write the module code to file
    let path = `${ __dirname }/../output/validators/${bundle}_${type}.js`;
    fs.writeFileSync(path, moduleCode);
    return path;
};

function compileConventionsValidator() {
    let schemasArgument = {
        schemas:[],
        code: {
            source: true
        }
    };
    let mapping = {};
    let conventions = fs.readdirSync( `${__dirname}/../output/collection/conventions`, (error, files) => {
        return files;
    } );
    // add all schemas into the object we need
    conventions.forEach( conventionName => {
        console.log(`Working on ${ conventionName }`);
        let schema = JSON.parse( fs.readFileSync( `${__dirname}/../output/collection/conventions/${conventionName}/schema.json` ) );
            schemasArgument.schemas.push(schema);
            mapping[conventionName] = schema.$id;
    } );
    let validatorObj = buildValidator(extraAJVArguments=schemasArgument);
    let moduleCode = standaloneCode(validatorObj, mapping);

    // Write the module code to file
    let path = `${ __dirname }/../output/validators/allConventions.js`;
    fs.writeFileSync(path, moduleCode);
};


/**
 * Will create a standalone validator that integrates all the type/bundle schemas mentioned in the schemata tree provided. It will be stored in the validators folder.
 * @param {schemataTree} schemataTree -- De Drupalized Schemas arranged in a tree branched by hierarchy, as FarmOS.js returns.
 */
function compileGeneralValidator(schemataTree) {
    let schemasArgument = {
        schemas:[],
        code: {
            source: true
        }
    };
    let mapping = {};
    // add all schemas into the object we need
    Object.keys(schemataTree).forEach( typeKey => {
        Object.keys(schemataTree[typeKey]).forEach( bundleKey => {
            let schema = schemataTree[typeKey][bundleKey];
            delete schema.$schema;
            schemasArgument.schemas.push(schema);
            mapping[`${typeKey}__${bundleKey}`] = schema.$id;
        } );
    } );
    let validatorObj = buildValidator(extraAJVArguments=schemasArgument);
    let moduleCode = standaloneCode(validatorObj, mapping);

    // Write the module code to file
    let path = `${ __dirname }/../output/validators/allFarmOSSchemas.js`;
    fs.writeFileSync(path, moduleCode);
};


function urlToStringPair( url ) {
    return url
        .replace( /(.*api\/)|(\/resource\/schema.*)/g, "" )
        .replace( "\/", "--" )
    ;
};

/**
 * API compose allows to create taxonomies by mentioning them as relationships, which do have a name attribute instead of the id (meaning: "find or create, and then link the appropiate id here, by name"). We will detect situations in which this feature was used and split the entities shaping them as regular, finished examples. Used inside the "exampleImporter" function.
 * @param {object} entity -- An entity from an API compose array.
 */
function apiComposeTaxonomiesTransformation(entity) {
    let relationshipAttributes = Object.keys( entity.relationships );

    let newEntities = [];

    relationshipAttributes.forEach( attr => {
        entity.relationships[attr].data = entity.relationships[attr].data
        .map( relationship => {
            if ( !relationship.id ) {
                let newEntityAttributes = Object.keys(relationship)
                .filter( d => d !== "type" )
                ;
                let newEntity = {
                    id: randomUUID(),
                    type: relationship.type,
                    attributes: {}
                };
                newEntityAttributes.forEach( newEntityAttr => {
                    // create the implied entity
                    newEntity.attributes[newEntityAttr] = relationship[newEntityAttr];
                    // erase from relationship, to make it schema compliant
                    delete relationship[newEntityAttr];
                } );
                newEntities.push(newEntity);
                relationship.id = newEntity.id;
            };
            return relationship;
        } )
        ;
    } );
    return({
        mainEntity: entity,
        newEntities: newEntities
    });
};



/**
 * Will import an example from the "examples" folder and organize the data inside it to help using it in testing a convention. Particularly, it will dissasemble all cases in which new taxonomies are expressed as relationships that have a name instead of an id and offer them as separate entities. The JSON file should contain an array from the SurveyStac's API compose functionality, containing entities generated by a survey submission.
 * @param {string} filename -- Name of a file, which should exclusively be stored as a JSON file in the "examples" folder.
 * @param {string} basePath -- Path under which examples are to be found. The repo has a default, but it can be changed for testing or for use on other folder structures.
 */
function exampleImporter(filename, basePath = `${__dirname}/../definitions/examples/raw_examples_and_scripts/`, schemataSource = `${__dirname}/../input/collection` ) {
    let rawData = JSON.parse( fs.readFileSync(`${basePath}/${filename}`) )
        .map( obj => {
            let output = obj.entity;
            // sometimes, "relationships" is stored inside "attributes", which is not schema compliant
            if (output.attributes.relationships) {
                output.relationships = output.attributes.relationships;
                delete output.attributes.relationships;
            };
            // topmost entity lacks an id sometimes, we need it for our schemas.
            if (!output.id) {
                output.id = randomUUID();
            };
            // Dates are parsed from JSON as big integers. We need to parse them into ISO String Dates. To make it easy, we first obtain the schema, which indicates which fields should be transformed.
            let schemaPath = `${schemataSource}/${ output.type.split("--")[0] }/${ output.type.split("--")[1] }/schema.json`;
            let schema = JSON.parse( fs.readFileSync(schemaPath, console.error) );
            parseDateFields(output, schema);

            return output;
        } )
    ;

    let newEntities = [];

    let entities = rawData.flatMap( entity => {
        let transformedData = apiComposeTaxonomiesTransformation( entity );
        return( [ transformedData.mainEntity, ... transformedData.newEntities ] );
    } );

    return entities;
};

/**
 * Examples from survey stack might mention entities that are not included. This function will compare all ids mentioned in relationships across all entities in the example against all entities that are described in detail.
 * @param {} entitiesArray
 * @returns {}
 */
function findMissingEntitiesInExample(entitiesArray) {
    let allIncludedIds = new Set();
    let allMentionedIds = new Set();
    let allMentionedEntities = [];
    entitiesArray.forEach( entity => {
        // store the ids of all objects this example details.
        allIncludedIds.add(entity.id);
        // store the ids of all objects mentioned in requirements, which might be more.
        if (entity.relationships) {
            Object.keys( entity.relationships ).forEach( relAttr => {
                let relatedIds = entity.relationships[ relAttr ].data.map( rel => rel.id );
                entity.relationships[relAttr].data.forEach( rel => {
                    if ( !allMentionedEntities.find( mention => mention.id == rel.id ) ) {
                        let mention = {
                            id: rel.id,
                            type: rel.type,
                            attribute: relAttr,
                            where: {
                                id: entity.id,
                                type: entity.type,
                                name: entity.attributes.name,
                                label: entity.attributes.label
                            }
                        };
                        allMentionedEntities.push(mention);
                    };
                } );
                relatedIds.forEach( id => allMentionedIds.add(id) );
            } );
        };
    } );
    let missing = Array.from(allMentionedIds)
        .filter( id => !allIncludedIds.has(id) )
    ;
    return {
        mentioned: Array.from(allMentionedIds),
        included: Array.from(allIncludedIds),
        mentionedNotIncluded: allMentionedEntities.filter( mention => missing.includes(mention.id) ),
        allMentioned: allMentionedEntities
    };
};

/**
 * JSON does not have a 'date' format and therefore, dates are parsed as long integers. In order to get proper, schema compliant dates, this function will search for all 'date' fields in the schema and parse the integers into string ISO dates as expected.
 * @param {Object} entity -- Entity parsed from a JSON text example.
 * @param {JSONSchema} schema -- JSON schema for the entity type, describing its fields.
 */
function parseDateFields(entity, schema) {
    let dateFields = Object.keys( schema.properties.attributes.properties )
        .filter( property => {
            return schema.properties.attributes.properties[property].format == "date-time";
        } )
    ;
    dateFields.forEach( property => {
        if (entity.attributes[property]) {
            entity.attributes[property] = new Date( 1000 * parseFloat( entity.attributes[property] ) ).toISOString();
        };
    } );
};

// exports.prepareDrupalSchemaForAJV = prepareDrupalSchemaForAJV;
exports.fixRelationshipDataField = fixRelationshipDataField;
exports.buildValidator = buildValidator;
exports.buildStaticValidator = buildStaticValidator;
exports.compileGeneralValidator = compileGeneralValidator;
exports.compileConventionsValidator = compileConventionsValidator;
exports.storeDeDrupalizedSchemas = storeDeDrupalizedSchemas;
exports.urlToStringPair = urlToStringPair;
exports.apiComposeTaxonomiesTransformation = apiComposeTaxonomiesTransformation;
exports.exampleImporter = exampleImporter;
exports.findMissingEntitiesInExample = findMissingEntitiesInExample;

const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let grazingConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--activity--grazing/object.json` ) );
let grazingConvention = new builder.ConventionSchema( grazingConventionSrc );

// reading the example
let grazingExample = exampleImporter(filename = "grazingLogSurvey_basic.json");

Object.keys(grazingConvention.schema.properties);

// empty object
let formattedExample = {
    grazing_log: {},
    log_category: {},
    animal_count: {},
    animal_weight: {},
    weight_unit:{},
    subpaddocks:{},
    area_quantity:{},
    area_unit:{},
    plant_asset: {}
};

//the first entity in the convention is the area quantity, assigning that here
formattedExample.area_quantity = grazingExample[0];

//second entity is the area unit (%)
formattedExample.area_unit = grazingExample[1];

//third entity is the grazing log
formattedExample.grazing_log = grazingExample[2];

//fourth entity is the plant asset
grazingExample[3].attributes.name = "tacit_plant_asset";
grazingExample[3].attributes.status = "active";
formattedExample.plant_asset = grazingExample[3];

//fifth entity is the log category
formattedExample.log_category = grazingExample[4];

//LOOK entities 6-10 just repeat the above four entities, ask Greg how we deal with having multiple quantities/logs/etc
// do we need to change the convention to duplicate attributes (have initial and final attributes)


// testing the example
grazingConvention.validExamples = [ formattedExample ];

let test = grazingConvention.testExamples();

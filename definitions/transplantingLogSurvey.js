// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 11, planting_div_veg
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');

//UUIDs
let transplantingLogUUID = randomUUID();
let logCategoryUUID = randomUUID();
let conventionUUID = randomUUID();
let percentageUnitUUID = randomUUID();
let areaPercentageUUID = randomUUID();

//Main entity
let transplantingLog = new builder.SchemaOverlay({
    typeAndBundle: 'log--transplanting',
    //what should this be named??
    name: 'transplanting',
    validExamples: [transplantingLogExample],
    erroredExamples: [transplantingLogError]    
});
transplantingLog.setMainDescription("If the crop was seeded and then moved a transplanting log is required.");

transplantingLog.setConstant({
    attribute:"status",
    value:"done",
    description: "The status should always be set to done to inherit the area."
});

transplantingLog.setConstant({
    attribute: "is_movement",
    value: "true"
});


//taxonomy term - log_category
let logCategory = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "log_category",
    validExamples: [logCategoryExample],
    erroredExamples: [logCategoryError],
});

//Convention
let transplantingConvention = new builder.ConventionSchema({
    title: "Diverse Vegetable Transplanting",
    version: "0.0.1",
    schemaName:"log--transplanting--div_veg_planting",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`
    ##Purpose\n
    A transplanting log is required when crops are seeded and then moved.\n
    ##Specification\n
    text\n`,
    // LOOK removed "OLD" to get the new, working example.
    validExamples: [],
    erroredExamples: []
});

//add attributes
transplantingConvention.addAttribute({ schemaOverlayObject:transplantingLog, attributeName: "transplanting_log", required: true});
transplantingConvention.addAttribute({ schemaOverlayObject:logCategory, attributeName: "log_category", required: true});
transplantingConvention.addAttribute({ schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "area_quantity", required: true});
transplantingConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "area_unit", required: true});
transplantingConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "plant_asset", required: true } );

//Add relationships
transplantingConvention.addRelationship({ containerEntity:"transplanting_log", relationName:"category", mentionedEntity:"log_category", required:true});
transplantingConvention.addRelationship({ containerEntity:"transplanting_log", relationName:"quantity", mentionedEntity:"area_quantity", required:true});
transplantingConvention.addRelationship({ containerEntity:"area_quantity", relationName:"units", mentionedEntity:"area_unit", required:true});
transplantingConvention.addRelationship( { containerEntity:"transplanting_log", relationName:"asset", mentionedEntity:"plant_asset", required: true } );

//Examples
//Transplanting Log
var transplantingLogExample = {
    id:transplantingLogUUID,
    attributes: {
        name: "example div veg transplanting log",
        status: "done",
        is_movement: "true"
    }
};
var transplantingLogError = {
    id:transplantingLogUUID,
    attributes: {
        name: "example div veg transplanting log",
        status: "pending",
        is_movement: "false"
    }
};

let plantAssetExampleAttributes = transplantingConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = transplantingConvention.overlays.plant_asset.validExamples[0].id;

//log category examples
var logCategoryExample = {
    id: logCategoryUUID,
    attributes: {
        name: "transplanting"
    }
};
var logCategoryError = {
    id: logCategoryUUID,
    attributes: {
        label: "activity"
    }
};

let transplantingConventionExample = {
    id: conventionUUID,
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    transplanting_log: {
        id:transplantingLogUUID,
        attributes: {
            name: "Example div veg transplanting log",
            is_movement: "true",
            status:"done"
        },
        relationships: {
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] }
        },
    },
    // This is the right, top level.
    log_category: logCategoryExample,
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    }
};

let transplantingConventionError = {
    id: conventionUUID,
    transplanting_log: {
        id: transplantingLogUUID,
        attributes: {
            name: "Example div veg transplanting log",
            status: "done"
        },
        relationships: {
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id: logCategoryUUID
                }
            ] }
        },
    },
    log_category: logCategoryExample
};

transplantingConvention.validExamples = [transplantingConventionExample];
transplantingConvention.erroredExamples = [transplantingConventionError];

let test = transplantingConvention.testExamples();
let storageOperation = transplantingConvention.store();


// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 32, Organic Matter Inputs
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

// ids for the examples
let organicMatterLogUUID = randomUUID();
let logCategoryUUID = randomUUID();
let moisturePercentageUUID = randomUUID();
let nitrogenPercentageUUID = randomUUID();
let phosphorusPercentageUUID = randomUUID();
let potassiumPercentageUUID = randomUUID();
let conventionUUID = randomUUID();
let areaPercentageUUID = randomUUID();
let percentageUnitUUID = randomUUID();
let organicMatterTypeUUID = randomUUID();
let organicMatterUnitUUID = randomUUID();

//examples
let organicMatterLogExample = {
    id:organicMatterLogUUID,
    attributes: {
        name: "example organic matter log",
        status:"done",
    }
};
let organicMatterLogError = {
    id:organicMatterLogUUID,
    attributes: {
        name: "example organic matter log",
        status:"false",
    }
};

let logCategoryExample = {
    id: logCategoryUUID,
    attributes: {
        name: "amendment"
    }
};
let logCategoryError = {
    id: logCategoryUUID,
    attributes: {
        name: "seeding"
    }
};
let moisturePercentageExample = {
    id: moisturePercentageUUID,
    attributes: {
        label: "moisture",
        measure: "water_content"
    }
};
let nitrogenPercentageExample = {
    id: nitrogenPercentageUUID,
    attributes: {
        label: "n",
        measure: "ratio"
    }
};
let phosphorusPercentageExample = {
    id: phosphorusPercentageUUID,
    attributes: {
        label: "p",
        measure: "ratio"
    }
};
let potassiumPercentageExample = {
    id: potassiumPercentageUUID,
    attributes: {
        label: "k",
        measure: "ratio"
    }
};

//Main entity
let organicMatterLog = new builder.SchemaOverlay({
    typeAndBundle: 'log--input',
    name: 'organic_matter',
    validExamples: [organicMatterLogExample],
    erroredExamples: [organicMatterLogError]    
});
organicMatterLog.setMainDescription("This log includes any organic matter inputs including mulch, compost, manure, etc.");

organicMatterLog.setConstant({
    attribute:"status",
    value:"done"
});

let logCategory = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "amendment",
    validExamples: [logCategoryExample],
    erroredExamples: [logCategoryError],
});

//quanities, will pull in % units
//moisture
let moisturePercentage = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "moisture_percentage",
    validExamples: [moisturePercentageExample]
    //erroredExamples: [areaPercentageError]
});
moisturePercentage.setMainDescription("The percent moisture present in the organic matter material.");
moisturePercentage.setConstant({
    attribute: "measure",
    //LOOK check that this is how value should be entered
    value:"water_content"
});
moisturePercentage.setConstant({
    attribute: "label",
    value:"moisture"
});

//n
let nitrogenPercentage = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "nitrogen_percentage",
    validExamples: [nitrogenPercentageExample]
    //erroredExamples: [areaPercentageError]
});
nitrogenPercentage.setMainDescription("The percent of nitrogen present in the organic matter material.");
nitrogenPercentage.setConstant({
    attribute: "measure",
    //LOOK check that this is how value should be entered
    value:"ratio"
});
nitrogenPercentage.setConstant({
    attribute: "label",
    value:"n"
});

//p
let phosphorusPercentage = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "phosphorus_percentage",
    validExamples: [phosphorusPercentageExample]
    //erroredExamples: [areaPercentageError]
});
phosphorusPercentage.setMainDescription("The percent of phosphorus present in the organic matter material.");
phosphorusPercentage.setConstant({
    attribute: "measure",
    //LOOK check that this is how value should be entered
    value:"ratio"
});
phosphorusPercentage.setConstant({
    attribute: "label",
    value:"p"
});

//k
let potassiumPercentage = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "potassium_percentage",
    validExamples: [potassiumPercentageExample]
    //erroredExamples: [areaPercentageError]
});
potassiumPercentage.setMainDescription("The percent of potassium present in the organic matter material.");
potassiumPercentage.setConstant({
    attribute: "measure",
    //LOOK check that this is how value should be entered
    value:"ratio"
});
potassiumPercentage.setConstant({
    attribute: "label",
    value:"k"
});

//organic matter material type name
let organicMatterType = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--material_type",
    name: "organic matter type",
    validExamples: [],
    erroredExamples: []
});
organicMatterType.setMainDescription("The type of organic matter is stored here.");

organicMatterType.setEnum({
    attribute: "name",
    valuesArray: [
        "biosolids",
        "farm_yard_waste_compost",
        "farm_waste_compost",
        "hay_mulch",
        "leaf_mulch",
        "manure_food_waste_compost",
        "manure_yard_waste_compost",
        "dairy_manure_compost",
        "dairy_manure_liquid",
        "manure_dairy",
        "manure_goat",
        "manure_hog_liquid",
        "manure_hog_solid",
        "manure_horse",
        "manure_horse_compost",
        "manure_mixed_compost",
        "manure_mixed_ruminant_compost",
        "manure_other",
        "manure_poultry",
        "manure_poultry_compost",
        "manure_sheep",
        "mushroom_compost",
        "straw_mulch",
        "wood_chips",
        "yard_waste_compost"
    ],
    description: "List of types of organic matter material."
});

//Fertilizer quantity units
////Examples
let organicMatterUnitExample = {
    id:organicMatterUnitUUID,
    attributes: {
        name: "lbs",
    }
};
let organicMatterUnitError = {
    id:organicMatterUnitUUID,
    attributes: {
        name: "ft",
    }
};

let organicMatterUnit = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "organic matter unit",
    validExamples: [ organicMatterUnitExample ],
    erroredExamples: [ organicMatterUnitError ]
});
organicMatterUnit.setEnum({
    attribute: "name",
    valuesArray: [
        "lbs",
        "in"
    ],
    description: "Organic matter is quantified in pounds or inches of material added."
});
//LOOK add description - Adie
organicMatterUnit.setMainDescription("");

//Convention
let organicMatterConvention = new builder.ConventionSchema({
    title: "Organic Matter Input",
    version: "0.0.1",
    schemaName:"log--input--organic_matter",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`
    ##Purpose\n
    This log records any organic matter inputs to a field or planting such as mulch, compost, manure, etc.\n
    ##Specification\n
    text\n`,
    validExamples: [organicMatterConventionExample],
    erroredExamples: [organicMatterConventionError]
});

//add attributes
organicMatterConvention.addAttribute( { schemaOverlayObject:organicMatterLog, attributeName: "organic_matter_log", required: true } );
organicMatterConvention.addAttribute( { schemaOverlayObject:logCategory, attributeName: "log_category", required: true } );
organicMatterConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "plant_asset", required: true } );
organicMatterConvention.addAttribute({ schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "area_quantity", required: false});
organicMatterConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "percentage_unit", required: false});
organicMatterConvention.addAttribute( { schemaOverlayObject:moisturePercentage, attributeName: "moisture", required: false } );
organicMatterConvention.addAttribute( { schemaOverlayObject:nitrogenPercentage, attributeName: "nitrogen", required: false } );
organicMatterConvention.addAttribute( { schemaOverlayObject:phosphorusPercentage, attributeName: "phosphorus", required: false } );
organicMatterConvention.addAttribute( { schemaOverlayObject:potassiumPercentage, attributeName: "potassium", required: false } );
organicMatterConvention.addAttribute( { schemaOverlayObject:"quantity--material--rate", attributeName: "rate", required: false } );
organicMatterConvention.addAttribute( { schemaOverlayObject: organicMatterType, attributeName: "organic_matter_type", required: false});
organicMatterConvention.addAttribute( { schemaOverlayObject: organicMatterUnit, attributeName: "organic_matter_unit", required: false});

//add relationships
organicMatterConvention.addRelationship( { containerEntity:"organic_matter_log" , relationName:"category" , mentionedEntity:"log_category" , required: true } );
organicMatterConvention.addRelationship( { containerEntity:"organic_matter_log", relationName:"asset", mentionedEntity:"plant_asset", required: true } );
organicMatterConvention.addRelationship( { containerEntity:"organic_matter_log" , relationName:"quantity" , mentionedEntity:"area_quantity" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"area_quantity" , relationName:"units" , mentionedEntity:"percentage_unit" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"organic_matter_log" , relationName:"quantity" , mentionedEntity:"moisture" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"moisture" , relationName:"units" , mentionedEntity:"percentage_unit" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"organic_matter_log" , relationName:"quantity" , mentionedEntity:"nitrogen" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"nitrogen" , relationName:"units" , mentionedEntity:"percentage_unit" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"organic_matter_log" , relationName:"quantity" , mentionedEntity:"phosphorus" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"phosphorus" , relationName:"units" , mentionedEntity:"percentage_unit" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"organic_matter_log" , relationName:"quantity" , mentionedEntity:"potassium" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"potassium" , relationName:"units" , mentionedEntity:"percentage_unit" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"organic_matter_log" , relationName:"quantity" , mentionedEntity:"rate" , required: false } );
organicMatterConvention.addRelationship( { containerEntity:"rate", relationName:"material_type" , mentionedEntity:"organic_matter_type" , required: true } );
organicMatterConvention.addRelationship( { containerEntity:"rate", relationName:"units" , mentionedEntity:"organic_matter_unit" , required: true } );

//examples
let plantAssetExampleAttributes = organicMatterConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = organicMatterConvention.overlays.plant_asset.validExamples[0].id;

//LOOK - need to review how to get attributes because valid examples array is [null]
let rateExampleAttributes = organicMatterConvention.overlays.rate.validExamples[0].attributes;
let rateUUID = organicMatterConvention.overlays.rate.validExamples[0].id;

var organicMatterConventionExample = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    organic_matter_log: {
        id: organicMatterLogUUID,
        attributes: {
            name: "example organic matter log",
            status: "done",
        },
        relationships: {
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] },
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: moisturePercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: nitrogenPercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: phosphorusPercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: potassiumPercentageUUID
                },
                {
                    type: "quantity--material",
                    id: rateUUID
                }
            ] }
        },
    },
    rate: {
        id: rateUUID,
        attributes: rateExampleAttributes,
        relationships: {
            material_type: { data: [
                {
                    type:"taxonomy_term--material_type",
                    id: organicMatterTypeUUID
                }
            ] },
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: organicMatterUnitUUID
                }
            ] }
        }
    },
    organic_matter_type:  {
        id: organicMatterTypeUUID,
        attributes: {
            name: "manure_dairy"
        },   
    },
    organic_matter_unit:  {
        id: organicMatterUnitUUID,
        attributes: {
            name: "lbs"
        },   
    },
    log_category: logCategoryExample,
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    percentage_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
    nitrogen: {
        id: nitrogenPercentageUUID,
        attributes: {
            label: "n",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    phosphorus: {
        id: phosphorusPercentageUUID,
        attributes: {
            label: "p",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    potassium: {
        id: potassiumPercentageUUID,
        attributes: {
            label: "k",
            measure: "ratio"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    }
};
var organicMatterConventionError = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    organic_matter_log: {
        id: organicMatterLogUUID,
        attributes: {
            name: "example organic matter log",
            status: "done",
        },
        relationships: {
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: moisturePercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: nitrogenPercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: phosphorusPercentageUUID
                },
                {
                    type: "quantity--standard",
                    id: potassiumPercentageUUID
                }
            ] }
        },
    },
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    }
};

organicMatterConvention.validExamples = [organicMatterConventionExample];
organicMatterConvention.erroredExamples = [organicMatterConventionError];

let test = organicMatterConvention.testExamples();
let storageOperation = organicMatterConvention.store();

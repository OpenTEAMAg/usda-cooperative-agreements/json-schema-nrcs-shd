// This file attempts to document cases in which schema compliant files are rejected due to conditions that could be expressed in a schema.
const Ajv = require("ajv");
const { buildValidator } = require("../src/schema_utilities.js");
const { randomUUID } = require('crypto');
const fetch = require("node-fetch");

require('dotenv').config({ path:`${__dirname}/../.env` });
let coffeeshopAggregatorURL = "https://surveystack.farmos.group/api/v2";

const aggregatorKey = process.env.FARMOS_KEY;

let ajv = buildValidator(); 

/**
 * Send entities to the test instance, mainly used for testing.
 * @param {} entities
 * @param {} farmOSKey
 */
async function sendentitiestotesting(entities, farmOSKey, aggregatorURL = coffeeshopAggregatorURL ) {
    let farmos_url = "ourscitest.farmos.net";
    let deleteHeaders = new fetch.Headers({
        'api-key': farmOSKey,
        'accept': 'application/json',
        'content-type': 'application/json'
    });
    let postOperations = entities.map( async entity => {
        let url = `${aggregatorURL}/farms/relay/${farmos_url}/api/${entity.data.type.replace('--','/')}`;
        let postOperation = await fetch(url,
                                          {
                                              method: 'POST',
                                              headers: deleteHeaders,
                                              body: JSON.stringify(entity)
                                          }
                                         );
        return postOperation;
    } );
    postOperations = await Promise.all(postOperations)
        .then( data => {return data;} )
    ;
    return postOperations;
};

let activityLogSchema = JSON.parse( fs.readFileSync(`${__dirname}/../input/collection/log/activity/schema.json`) );

/// EXAMPLE
/// Apparently, the log status should be an enum, but it isn't. It is not getting rejected, though.
let activityLogExample = {
    data:{
        type: 'log--activity',
    id: '2e4839b2-c95e-4e90-9c24-9634356e14a4',
    attributes: {
       status: 'pronto'
    }
} };


ajv.addSchema(activityLogSchema);
let validateActivityLog = ajv.compile(activityLogSchema);

let valid = validateActivityLog(activityLogExample.data);
if (!valid) console.log(validateActivityLog.errors);

let activityLogSubmissionOperation = sendentitiestotesting(
    entities = [activityLogExample],
    farmOSKey = aggregatorKey
);

activityLogSubmissionOperation.then( upload => {
    console.log(`According to the schema, this ${activityLogExample.data.type} is ${valid}, though the submissions operation status is ${upload[0].statusText}`);
} );


/// EXAMPLE
/// Quantities have an attribute called 'measure' which is schematized as an arbitrary string but is really an enum.
let quantitySchema = JSON.parse( fs.readFileSync(`${__dirname}/../input/collection/quantity/standard/schema.json`) );

let quantityExample = {
    data:{
        type: 'quantity--standard',
        id: '2e4839b2-c95e-4e90-9d24-9634356e14a4',
        attributes: {
            measure: 'width'
        }
    } };


ajv.addSchema(quantitySchema);
let validateQuantity = ajv.compile(quantitySchema);

let quantityValid = validateQuantity(quantityExample.data);
if (!quantityValid) console.log(validateQuantity.errors);

let quantitySubmissionOperation = sendentitiestotesting(
    entities = [quantityExample],
    farmOSKey = aggregatorKey
);

quantitySubmissionOperation.then( upload => {
    console.log(`According to the schema, this example's compliance with the schema for ${quantityExample.data.type} is ${quantityValid}, though the submissions operation status is ${upload[0].statusText}`);
} );


/// EXAMPLE
/// Apparently, the log status should be an enum, but it isn't. It is not getting rejected, though.
let labTestLogExample = {
    data:{
        type: 'log--lab_test',
        id: '3e4839b2-c95e-4e90-9c24-9634356e14a5',
        attributes: {
            status: 'pronto'
        }
    } };
let labTestLogSchema = JSON.parse( fs.readFileSync(`${__dirname}/../input/collection/log/lab_test/schema.json`) );

ajv.addSchema(labTestLogSchema);
let validateLabTestLog = ajv.compile(labTestLogSchema);

let validLabTestLog = validateLabTestLog(labTestLogExample.data);
if (!validLabTestLog) console.log(validateLabTestLog.errors);

let labTestLogSubmissionOperation = sendentitiestotesting(
    entities = [labTestLogExample],
    farmOSKey = aggregatorKey
);

labTestLogSubmissionOperation.then( upload => {
    console.log(`According to the schema, this ${labTestLogExample.data.type} is ${validLabTestLog}, though the submissions operation status is ${upload[0].statusText}`);
} );
